import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { mnCtx } from 'fe/lib/graphql/ctx';
import Maybe from 'graphql/tsutils/Maybe';
import { Comment, Community, Thread, ActivityVerb } from 'graphql/types.generated';
import { useMemo } from 'react';
import * as GQL from './useReplyComment.generated';

// instance-timeline	add on top *
import * as instanceActivityTimelineOutboxCache from 'fe/activities/outbox/instance/cache';

// profile-timeline (my outbox)	add on top *
import * as userActivityTimelineOutboxCache from 'fe/activities/outbox/user/cache';

// my-moodlenet-timeline (my inbox)	add on top *
import * as myActivityTimelineInboxCache from 'fe/activities/inbox/my/cache';

// thread's comment list	add to bottom if last page
import * as threadCommentListCache from 'fe/comment/thread/cache';
import { fakeActivityPreviewFragment } from 'fe/activities/helpers/fakeActivity';
import { useMe } from 'fe/session/useMe';

export const useReplyComment = (
  comment: Maybe<{
    id: Comment['id'];
    thread: Maybe<{ id: Thread['id'] }>;
  }>,
  communityId: Maybe<Community['id']>,
  to: Maybe<string>
) => {
  const [createReplyMut, status] = GQL.useReplyMutation();
  const { me } = useMe();
  const mutating = status.loading;

  const reply = useCallOrNotifyMustLogin(
    async (content: string) => {
      if (mutating || !comment || !comment.thread) {
        return;
      }
      const threadId = comment.thread.id;
      return createReplyMut({
        variables: {
          threadId,
          inReplyToCommentId: comment.id,
          comment: { content }
        },
        context: mnCtx<GQL.ReplyMutation>({
          successMsg: () => `Sent reply ${to ? `@${to} ` : ''} correctly`
        }),

        update: (proxy, result) => {
          if (!(result.data?.createReply && me?.user)) {
            return;
          }
          const comment = result.data.createReply;
          const activity = fakeActivityPreviewFragment({
            context: comment,
            user: me.user,
            verb: ActivityVerb.Created
          });
          instanceActivityTimelineOutboxCache.addOne(proxy, { activity });
          userActivityTimelineOutboxCache.addOne(proxy, { activity, userId: me.user.id });
          myActivityTimelineInboxCache.addOne(proxy, { activity });
          threadCommentListCache.addOne(proxy, { comment, threadId });
        }
      });
    },
    [mutating, communityId, to]
  );

  return useMemo(
    () => ({
      reply
    }),
    [reply]
  );
};
