import { DataProxy } from 'apollo-cache';
import {
  InstanceOutboxActivitiesDocument,
  InstanceOutboxActivitiesQuery,
  InstanceOutboxActivitiesQueryVariables
} from '../useInstanceOutboxActivities.generated';
import { ActivityPreviewFragment } from 'ctrl/modules/previews/activity/ActivityPreview.generated';

type Data = {
  activity: ActivityPreviewFragment;
};

export const addOne = (proxy: DataProxy, { activity }: Data) => {
  const cache = proxy.readQuery<
    InstanceOutboxActivitiesQuery,
    InstanceOutboxActivitiesQueryVariables
  >({
    query: InstanceOutboxActivitiesDocument,
    variables: {}
  });
  if (!cache?.instance?.outbox?.edges) {
    return;
  }
  const newEdges = [activity, ...cache.instance.outbox.edges];
  proxy.writeQuery<InstanceOutboxActivitiesQuery, InstanceOutboxActivitiesQueryVariables>({
    query: InstanceOutboxActivitiesDocument,
    variables: {},
    data: {
      ...cache,
      instance: {
        ...cache.instance,
        outbox: {
          ...cache.instance.outbox,
          edges: newEdges
        }
      }
    }
  });
};
