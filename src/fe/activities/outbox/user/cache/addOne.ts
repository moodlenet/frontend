import { DataProxy } from 'apollo-cache';
import {
  UserOutboxActivitiesDocument,
  UserOutboxActivitiesQuery,
  UserOutboxActivitiesQueryVariables
} from '../useUserOutboxActivities.generated';
import { ActivityPreviewFragment } from 'ctrl/modules/previews/activity/ActivityPreview.generated';
import { User } from 'graphql/types.generated';

type Data = {
  activity: ActivityPreviewFragment;
  userId: User['id'];
};

export const addOne = (proxy: DataProxy, { activity, userId }: Data) => {
  const cache = proxy.readQuery<UserOutboxActivitiesQuery, UserOutboxActivitiesQueryVariables>({
    query: UserOutboxActivitiesDocument,
    variables: { userId }
  });
  if (!cache?.user?.outbox?.edges) {
    return;
  }
  const newEdges = [activity, ...cache.user.outbox.edges];
  proxy.writeQuery<UserOutboxActivitiesQuery, UserOutboxActivitiesQueryVariables>({
    query: UserOutboxActivitiesDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        outbox: {
          ...cache.user.outbox,
          edges: newEdges
        }
      }
    }
  });
};
