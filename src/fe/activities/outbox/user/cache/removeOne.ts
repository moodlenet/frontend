import { DataProxy } from 'apollo-cache';
import {
  UserOutboxActivitiesDocument,
  UserOutboxActivitiesQuery,
  UserOutboxActivitiesQueryVariables
} from '../useUserOutboxActivities.generated';
import { User, Activity } from 'graphql/types.generated';

type Data = {
  userId: User['id'];
} & (
  | {
      contextId: Exclude<Activity['context'], null | undefined>['id'];
    }
  | {
      activityId: Activity['id'];
    }
);

export const removeOne = (proxy: DataProxy, data: Data) => {
  const { userId } = data;
  const contextId = 'contextId' in data ? data.contextId : null;
  const activityId = 'activityId' in data ? data.activityId : null;
  if (!(contextId || activityId)) {
    return;
  }
  const cache = proxy.readQuery<UserOutboxActivitiesQuery, UserOutboxActivitiesQueryVariables>({
    query: UserOutboxActivitiesDocument,
    variables: { userId }
  });
  if (!cache?.user?.outbox?.edges) {
    return;
  }
  const newEdges = cache.user.outbox.edges.filter(activity => {
    if (activityId) {
      return activity.id !== activityId;
    } else if (activity.context) {
      const _ctxId =
        activity.context.__typename === 'User' ? activity.context.userId : activity.context.id;
      return _ctxId !== contextId;
    } else {
      return true;
    }
  });
  proxy.writeQuery<UserOutboxActivitiesQuery, UserOutboxActivitiesQueryVariables>({
    query: UserOutboxActivitiesDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        outbox: {
          ...cache.user.outbox,
          edges: newEdges
        }
      }
    }
  });
};
