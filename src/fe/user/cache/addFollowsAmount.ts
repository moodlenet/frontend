import { DataProxy } from 'apollo-cache';
import { UserDataDocument, UserDataQuery, UserDataQueryVariables } from '../useUser.generated';
import { User, Community, Collection } from 'graphql/types.generated';

type OnActor = Exclude<(User | Community | Collection)['__typename'], undefined>;
type Data = {
  userId: User['id'];
  on: OnActor;
  amount: number;
};
const FIELD_MAP = {
  Collection: 'collectionFollows',
  Community: 'communityFollows',
  User: 'userFollows'
} as const;
export const addFollowsAmount = (proxy: DataProxy, { userId, amount, on }: Data) => {
  if (!amount) {
    return;
  }
  const cache = proxy.readQuery<UserDataQuery, UserDataQueryVariables>({
    query: UserDataDocument,
    variables: { userId }
  });
  if (!cache?.user) {
    return;
  }

  const replaceField = FIELD_MAP[on];
  const oldCountField = cache.user[replaceField];
  if (!oldCountField) {
    return;
  }
  const newField = cache.user[replaceField] && {
    ...oldCountField,
    totalCount: oldCountField.totalCount + Math.ceil(amount)
  };

  proxy.writeQuery<UserDataQuery, UserDataQueryVariables>({
    query: UserDataDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        [replaceField]: newField
      }
    }
  });
};
