import { UnfollowMutation } from 'fe/mutation/follow/useMutateFollow.generated';

export const optimisticUnfollow = (
  __typename: any,
  id: string,
  followerCount: number
): UnfollowMutation => ({
  __typename: 'RootMutationType',
  delete: {
    __typename: 'Follow',
    context: {
      __typename,
      id,
      myFollow: null,
      followerCount: followerCount - 1
    }
  }
});
