import { FlagMutation } from 'fe/mutation/flag/useMutateFlag.generated';
import { OPTIMISTIC_ID_STRING } from 'fe/lib/helpers/mutations';

export const optimisticFlag = (__typename: any, id: string): FlagMutation => ({
  __typename: 'RootMutationType',
  createFlag: {
    __typename: 'Flag',
    context: {
      __typename,
      id,
      myFlag: { __typename: 'Flag', id: OPTIMISTIC_ID_STRING }
    }
  }
});
