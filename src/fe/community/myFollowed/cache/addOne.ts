import { DataProxy } from 'apollo-cache';
import {
  MyCommunityFollowsDataFragment,
  MyCommunityFollowsDocument,
  MyCommunityFollowsQuery,
  MyCommunityFollowsQueryVariables
} from '../myFollowedCommunities.generated';

type Data = {
  communityFollow: MyCommunityFollowsDataFragment;
};

export const addOne = (proxy: DataProxy, { communityFollow }: Data) => {
  const cache = proxy.readQuery<MyCommunityFollowsQuery, MyCommunityFollowsQueryVariables>({
    query: MyCommunityFollowsDocument
  });
  if (!cache?.me?.user.communityFollows?.edges) {
    return;
  }
  const oldEdges = cache.me.user.communityFollows.edges;
  type Edges = typeof oldEdges;
  const edges: Edges = [communityFollow, ...oldEdges];
  proxy.writeQuery<MyCommunityFollowsQuery, MyCommunityFollowsQueryVariables>({
    query: MyCommunityFollowsDocument,
    data: {
      ...cache,
      me: {
        ...cache.me,
        user: {
          ...cache.me.user,
          communityFollows: {
            ...cache.me.user.communityFollows,
            edges
          }
        }
      }
    }
  });
};
