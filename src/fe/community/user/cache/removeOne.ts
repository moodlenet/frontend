import { DataProxy } from 'apollo-cache';
import {
  UserFollowedCommunitiesDocument,
  UserFollowedCommunitiesQuery,
  UserFollowedCommunitiesQueryVariables,
  UserCommunityFollowFragment
} from '../useUserFollowedCommunities.generated';
import { User } from 'graphql/types.generated';

type Data = {
  communityFollowId: UserCommunityFollowFragment['id'];
  userId: User['id'];
};

export const removeOne = (proxy: DataProxy, { communityFollowId, userId }: Data) => {
  const cache = proxy.readQuery<
    UserFollowedCommunitiesQuery,
    UserFollowedCommunitiesQueryVariables
  >({
    query: UserFollowedCommunitiesDocument,
    variables: { userId }
  });
  if (!cache?.user?.communityFollows?.edges) {
    return;
  }
  const edges = cache.user.communityFollows.edges.filter(follow => follow.id !== communityFollowId);
  proxy.writeQuery<UserFollowedCommunitiesQuery, UserFollowedCommunitiesQueryVariables>({
    query: UserFollowedCommunitiesDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        communityFollows: {
          ...cache.user.communityFollows,
          edges
        }
      }
    }
  });
};
