import { DataProxy } from 'apollo-cache';
import {
  MyCollectionFollowsDataFragment,
  MyCollectionFollowsDocument,
  MyCollectionFollowsQuery,
  MyCollectionFollowsQueryVariables
} from '../myFollowedCollections.generated';

type Data = {
  collectionFollow: MyCollectionFollowsDataFragment;
};

export const addOne = (proxy: DataProxy, { collectionFollow }: Data) => {
  const cache = proxy.readQuery<MyCollectionFollowsQuery, MyCollectionFollowsQueryVariables>({
    query: MyCollectionFollowsDocument
  });
  if (!cache?.me?.user.collectionFollows?.edges) {
    return;
  }
  const oldEdges = cache.me.user.collectionFollows.edges;
  type Edges = typeof oldEdges;
  const edges: Edges = [collectionFollow, ...oldEdges];
  proxy.writeQuery<MyCollectionFollowsQuery, MyCollectionFollowsQueryVariables>({
    query: MyCollectionFollowsDocument,
    data: {
      ...cache,
      me: {
        ...cache.me,
        user: {
          ...cache.me.user,
          collectionFollows: {
            ...cache.me.user.collectionFollows,
            edges
          }
        }
      }
    }
  });
};
