import { DataProxy } from 'apollo-cache';
import {
  MyCollectionFollowsDataFragment,
  MyCollectionFollowsQuery,
  MyCollectionFollowsQueryVariables,
  MyCollectionFollowsDocument
} from '../myFollowedCollections.generated';

type Data = {
  collectionFollowId: MyCollectionFollowsDataFragment['id'];
};

export const removeOne = (proxy: DataProxy, { collectionFollowId }: Data) => {
  const cache = proxy.readQuery<MyCollectionFollowsQuery, MyCollectionFollowsQueryVariables>({
    query: MyCollectionFollowsDocument
  });
  if (!cache?.me?.user.collectionFollows?.edges) {
    return;
  }
  const edges = cache.me.user.collectionFollows.edges.filter(
    follow => follow.id !== collectionFollowId
  );
  proxy.writeQuery<MyCollectionFollowsQuery, MyCollectionFollowsQueryVariables>({
    query: MyCollectionFollowsDocument,
    data: {
      ...cache,
      me: {
        ...cache.me,
        user: {
          ...cache.me.user,
          collectionFollows: {
            ...cache.me.user.collectionFollows,
            edges
          }
        }
      }
    }
  });
};
