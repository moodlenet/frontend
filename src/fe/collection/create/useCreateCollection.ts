import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { fakeActivityPreviewFragment } from 'fe/activities/helpers/fakeActivity';
import * as myInboxCache from 'fe/activities/inbox/my/cache';
import * as communityOutboxCache from 'fe/activities/outbox/community/cache';
import * as instanceOutboxCache from 'fe/activities/outbox/instance/cache';
import * as userOutboxCache from 'fe/activities/outbox/user/cache';
import * as allCollectionsCache from 'fe/collection/all/cache';
import * as communityCollectionsCache from 'fe/collection/community/cache';
import * as userCollectionFollowsCache from 'fe/collection/user/cache';
import { getMaybeUploadInput } from 'fe/mutation/upload/getUploadInput';
import { useMe } from 'fe/session/useMe';
import * as userPageCache from 'fe/user/cache';
import Maybe from 'graphql/tsutils/Maybe';
import { ActivityVerb, CollectionInput, Community } from 'graphql/types.generated';
import { useMemo } from 'react';
import { useCreateCollectionMutation } from './useCreateCollection.generated';

export interface CreateCollection {
  collection: CollectionInput;
  icon: Maybe<File | string>;
}
export const useCreateCollection = (communityId: Community['id']) => {
  const [createMut, createMutStatus] = useCreateCollectionMutation();
  const { me } = useMe();

  const create = useCallOrNotifyMustLogin(
    async ({ collection, icon }: CreateCollection) => {
      if (createMutStatus.loading) {
        return;
      }

      return createMut({
        variables: {
          communityId: communityId,
          icon: getMaybeUploadInput(icon, null),
          collection: {
            name: collection.name,
            summary: collection.summary,
            preferredUsername: collection.preferredUsername
          }
        },
        update: (proxy, result) => {
          if (!(result.data?.createCollection && me?.user)) {
            return;
          }
          const meUserId = me.user.id;
          const collection = result.data.createCollection;

          // all-collections	add on top
          allCollectionsCache.addOne(proxy, { collection });

          if (collection.creator?.id === meUserId) {
            if (collection.myFollow) {
              const collectionFollow = {
                ...collection.myFollow,
                context: collection
              };
              // my-followed-collections	add on top
              userCollectionFollowsCache.addOrSubstituteOne(proxy, {
                collectionFollow,
                userId: meUserId
              });
            }
          }
          const activity = fakeActivityPreviewFragment({
            context: collection,
            user: me.user,
            verb: ActivityVerb.Created
          });
          if (collection.creator?.id) {
            // my-followed-collections count	+1
            userPageCache.addFollowsAmount(proxy, {
              userId: collection.creator.id,
              on: 'Collection',
              amount: 1
            });
            // profile-timeline (my outbox)	add on top *
            userOutboxCache.addOne(proxy, { activity, userId: collection.creator.id });
          }
          // my-moodlenet-timeline (my inbox)	add on top *
          myInboxCache.addOne(proxy, { activity });

          // instance-timeline	add on top *
          instanceOutboxCache.addOne(proxy, { activity });

          // community's collections list	add on top
          communityCollectionsCache.addOne(proxy, { communityId, collection });

          // community's collections count	+1
          // this gets update by response : CollectionPageDataFragment-> HeroCollectionDataFragment
          // communityPreviewCache.amountCountAdd(proxy, {
          //   amount: 1,
          //   communityId,
          //   on: 'collectionCount'
          // });

          // community's timeline	add on top *
          communityOutboxCache.addOne(proxy, { activity, communityId });
        }
      });
    },
    [communityId, createMutStatus, createMut]
  );
  return useMemo(() => {
    return {
      create
    };
  }, [create]);
};
