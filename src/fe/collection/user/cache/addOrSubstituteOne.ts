import { DataProxy } from 'apollo-cache';
import {
  UserFollowedCollectionsDocument,
  UserFollowedCollectionsQuery,
  UserFollowedCollectionsQueryVariables,
  UserCollectionFollowFragment
} from '../useUserFollowedCollections.generated';
import { User } from 'graphql/types.generated';

type Data = {
  collectionFollow: UserCollectionFollowFragment;
  userId: User['id'];
};

export const addOrSubstituteOne = (proxy: DataProxy, { collectionFollow, userId }: Data) => {
  const cache = proxy.readQuery<
    UserFollowedCollectionsQuery,
    UserFollowedCollectionsQueryVariables
  >({
    query: UserFollowedCollectionsDocument,
    variables: { userId }
  });
  if (!cache?.user?.collectionFollows?.edges) {
    return;
  }

  let newEdges = [...cache.user.collectionFollows.edges];

  const wasFollowedIndex = cache.user.collectionFollows.edges.findIndex(
    _ =>
      _.context.__typename === 'Collection' &&
      collectionFollow.context.__typename === 'Collection' &&
      collectionFollow.context.id === _.context.id
  );
  if (wasFollowedIndex > -1) {
    newEdges[wasFollowedIndex] = collectionFollow;
  } else {
    newEdges = [collectionFollow, ...newEdges];
  }

  proxy.writeQuery<UserFollowedCollectionsQuery, UserFollowedCollectionsQueryVariables>({
    query: UserFollowedCollectionsDocument,
    variables: { userId },
    data: {
      ...cache,
      user: {
        ...cache.user,
        collectionFollows: {
          ...cache.user.collectionFollows,
          edges: newEdges
        }
      }
    }
  });
};
