import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { removeOne } from 'fe/resource/collection/cache/removeOne';
import { Resource, Collection } from 'graphql/types.generated';
import { useMemo } from 'react';
import { useDeleteResourceMutation } from './useDeleteResource.generated';
import Maybe from 'graphql/tsutils/Maybe';

export type Data = {
  resourceId: Resource['id'];
  collectionId: Maybe<Collection['id']>;
};
export const useDeleteResource = ({ resourceId, collectionId }: Data) => {
  const [deleteMut, deleteMutStatus] = useDeleteResourceMutation();
  const deleteResource = useCallOrNotifyMustLogin(async () => {
    if (deleteMutStatus.loading) {
      return;
    }
    return deleteMut({
      variables: {
        resourceId
      },
      optimisticResponse: collectionId
        ? {
            __typename: 'RootMutationType',
            delete: {
              __typename: 'Resource',
              id: resourceId,
              collection: {
                __typename: 'Collection',
                id: collectionId
              }
            }
          }
        : undefined,
      // refetchQueries: [CollectionResourcesQueryName],
      update: (proxy, result) => {
        if (!(result.data?.delete?.__typename === 'Resource' && result.data.delete.collection)) {
          return;
        }
        const resourceId = result.data.delete.id;
        const collectionId = result.data.delete.collection.id;
        removeOne(proxy, { resourceId, collectionId });
      }
    });
  }, [resourceId, deleteMutStatus, deleteMut]);

  return useMemo(() => {
    return {
      deleteResource,
      loading: deleteMutStatus.loading
    };
  }, [deleteMutStatus.loading, deleteResource]);
};
