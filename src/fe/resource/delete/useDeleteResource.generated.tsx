import * as Types from '../../../graphql/types.generated';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';

export type DeleteResourceMutationVariables = {
  resourceId: Types.Scalars['String']
};


export type DeleteResourceMutation = (
  { __typename: 'RootMutationType' }
  & { delete: Types.Maybe<{ __typename: 'Collection' } | { __typename: 'Comment' } | { __typename: 'Community' } | { __typename: 'Feature' } | { __typename: 'Flag' } | { __typename: 'Follow' } | { __typename: 'Like' } | (
    { __typename: 'Resource' }
    & Pick<Types.Resource, 'id'>
    & { collection: Types.Maybe<(
      { __typename: 'Collection' }
      & Pick<Types.Collection, 'id'>
    )> }
  ) | { __typename: 'Thread' } | { __typename: 'User' }> }
);


export const DeleteResourceDocument = gql`
    mutation deleteResource($resourceId: String!) {
  delete(contextId: $resourceId) {
    ... on Resource {
      id
      collection {
        id
      }
    }
  }
}
    `;
export type DeleteResourceMutationFn = ApolloReactCommon.MutationFunction<DeleteResourceMutation, DeleteResourceMutationVariables>;

/**
 * __useDeleteResourceMutation__
 *
 * To run a mutation, you first call `useDeleteResourceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useDeleteResourceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [deleteResourceMutation, { data, loading, error }] = useDeleteResourceMutation({
 *   variables: {
 *      resourceId: // value for 'resourceId'
 *   },
 * });
 */
export function useDeleteResourceMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<DeleteResourceMutation, DeleteResourceMutationVariables>) {
        return ApolloReactHooks.useMutation<DeleteResourceMutation, DeleteResourceMutationVariables>(DeleteResourceDocument, baseOptions);
      }
export type DeleteResourceMutationHookResult = ReturnType<typeof useDeleteResourceMutation>;
export type DeleteResourceMutationResult = ApolloReactCommon.MutationResult<DeleteResourceMutation>;
export type DeleteResourceMutationOptions = ApolloReactCommon.BaseMutationOptions<DeleteResourceMutation, DeleteResourceMutationVariables>;


export interface DeleteResourceMutationOperation {
  operationName: 'deleteResource'
  result: DeleteResourceMutation
  variables: DeleteResourceMutationVariables
  type: 'mutation'
}
export const DeleteResourceMutationName:DeleteResourceMutationOperation['operationName'] = 'deleteResource'

export const DeleteResourceMutationRefetch = (
  variables:DeleteResourceMutationVariables, 
  context?:any
)=>({
  query:DeleteResourceDocument,
  variables,
  context
})
      
