import { Resource } from 'graphql/types.generated';
import { useResourcePreviewQuery } from './useResourcePreview.generated';
import { useMemo } from 'react';
import { useLikeContext } from 'fe/context/like/useLikeContext';
import { useDeleteResource } from '../delete/useDeleteResource';
import { useMe } from 'fe/session/useMe';
import { useEditResource } from '../edit/useEditResource';

export const useResourcePreview = (resourceId: Resource['id']) => {
  const resourcePreviewQ = useResourcePreviewQuery({
    variables: { resourceId }
  });
  const resource = resourcePreviewQ.data?.resource;
  const { isAdmin, me } = useMe();
  const { edit, loading: isEditing } = useEditResource(resourceId);
  const canEdit = isAdmin || !!(me && resource?.creator?.id === me.user.id);
  const canDelete = isAdmin;

  const { deleteResource, loading: isDeleting } = useDeleteResource({
    resourceId,
    collectionId: resource?.collection?.id
  });
  const { toggleLike } = useLikeContext(
    resourcePreviewQ.data?.resource?.id,
    resourcePreviewQ.data?.resource?.myLike,
    resourcePreviewQ.data?.resource?.likerCount,
    'Resource'
  );

  return useMemo(() => {
    return {
      resource,
      toggleLike,
      canDelete,
      del: deleteResource,
      canEdit,
      edit,
      isDeleting,
      isEditing
    };
  }, [canDelete, canEdit, deleteResource, edit, isDeleting, isEditing, resource, toggleLike]);
};
