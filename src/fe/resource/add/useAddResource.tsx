import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { getMaybeUploadInput, getUploadInput } from 'fe/mutation/upload/getUploadInput';
import Maybe from 'graphql/tsutils/Maybe';
import { Collection, ResourceInput, ActivityVerb } from 'graphql/types.generated';
import { useMemo } from 'react';
import * as GQL from './useAddResource.generated';

// instance-timeline	add on top *
import * as instanceActivitiesTimelineCache from 'fe/activities/outbox/instance/cache';

// my-moodlenet-timeline (my inbox)	add on top *
import * as myActivitiesTimelineCache from 'fe/activities/inbox/my/cache';

// collection's resources list	add on top
import * as collectionResourcesCache from 'fe/resource/collection/cache';
import { fakeActivityPreviewFragment } from 'fe/activities/helpers/fakeActivity';
import { useMe } from 'fe/session/useMe';

export interface AddResource {
  collectionId: Collection['id'];
  resource: ResourceInput;
  content: File | string;
  icon: Maybe<File | string>;
}
export const useAddResource = () => {
  const [createResource, createResourceStatus] = GQL.useAddResourceCreateResourceMutation();
  const { me } = useMe();
  const create = useCallOrNotifyMustLogin(
    async ({ collectionId, content, icon, resource }: AddResource) => {
      if (createResourceStatus.loading) {
        return;
      }

      return createResource({
        variables: {
          collectionId: collectionId,
          resource,
          content: getUploadInput(content),
          icon: getMaybeUploadInput(icon, null)
        },
        update: (proxy, result) => {
          if (!(result.data?.createResource && me?.user)) {
            return;
          }
          const meUserId = me.user.id;
          const resource = result.data.createResource;
          const activity = fakeActivityPreviewFragment({
            context: resource,
            user: me.user,
            verb: ActivityVerb.Created
          });
          // instance-timeline	add on top *
          instanceActivitiesTimelineCache.addOne(proxy, { activity });

          if (resource.creator?.id === meUserId) {
            // my-moodlenet-timeline (my inbox)	add on top *
            myActivitiesTimelineCache.addOne(proxy, { activity });
          }

          // collection's resources list	add on top
          collectionResourcesCache.addOne(proxy, { collectionId, resource });

          // collection's resources list count
          // is updated by result type
        }
      });
    },
    [createResourceStatus, createResource]
  );
  return useMemo(() => {
    return {
      create
    };
  }, [create]);
};
