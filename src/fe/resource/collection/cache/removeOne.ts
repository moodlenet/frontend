import { DataProxy } from 'apollo-cache';
import { Resource, Collection } from 'graphql/types.generated';
import {
  CollectionResourcesQuery,
  CollectionResourcesQueryVariables,
  CollectionResourcesDocument
} from '../useCollectionResources.generated';

type Data = {
  resourceId: Resource['id'];
  collectionId: Collection['id'];
};

export const removeOne = (proxy: DataProxy, { collectionId, resourceId }: Data) => {
  const cache = proxy.readQuery<CollectionResourcesQuery, CollectionResourcesQueryVariables>(
    {
      variables: { collectionId },
      query: CollectionResourcesDocument
    },
    true
  );
  if (!cache?.collection?.resources?.edges) {
    return;
  }
  const edges = cache.collection.resources.edges.filter(edge => edge.id !== resourceId);
  proxy.writeQuery<CollectionResourcesQuery, CollectionResourcesQueryVariables>({
    query: CollectionResourcesDocument,
    variables: { collectionId },
    data: {
      ...cache,
      collection: {
        ...cache.collection,
        resources: { ...cache.collection.resources, edges }
      }
    }
  });
};
