import { getMaybeUploadInput } from 'fe/mutation/upload/getUploadInput';
import Maybe from 'graphql/tsutils/Maybe';
import { Resource, ResourceInput } from 'graphql/types.generated';
import { useCallOrNotifyMustLogin } from 'ctrl/lib/notifyMustLogin';
import { useMemo } from 'react';
import { useEditResourceDataQuery, useEditResourceMutation } from './useEditResource.generated';

export interface UpdateResource {
  resource: ResourceInput;
  icon: Maybe<File | string>;
}
export const useEditResource = (resourceId: Resource['id']) => {
  const [editMut, editMutStatus] = useEditResourceMutation();
  const resourceEditQ = useEditResourceDataQuery({
    variables: { resourceId }
  });

  const edit = useCallOrNotifyMustLogin(
    async ({ resource, icon }: UpdateResource) => {
      if (editMutStatus.loading) {
        return;
      }
      return editMut({
        variables: {
          resourceId,
          icon: getMaybeUploadInput(icon, resourceEditQ.data?.resource?.icon?.url),
          resource
        }
      });
    },
    [resourceId, editMutStatus, editMut, resourceEditQ]
  );

  return useMemo(() => {
    const resource = resourceEditQ.data?.resource;
    return {
      edit,
      resource,
      loading: editMutStatus.loading
    };
  }, [edit, editMutStatus.loading, resourceEditQ.data]);
};
