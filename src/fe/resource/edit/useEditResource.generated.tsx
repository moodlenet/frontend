import * as Types from '../../../graphql/types.generated';

import gql from 'graphql-tag';
import * as ApolloReactCommon from '@apollo/react-common';
import * as ApolloReactHooks from '@apollo/react-hooks';

export type EditResourceQueryDataFragment = (
  { __typename: 'Resource' }
  & Pick<Types.Resource, 'id' | 'language' | 'level' | 'license' | 'name' | 'subject' | 'summary' | 'format' | 'type'>
  & { icon: Types.Maybe<(
    { __typename: 'Content' }
    & Pick<Types.Content, 'id' | 'url'>
    & { upload: Types.Maybe<(
      { __typename: 'ContentUpload' }
      & Pick<Types.ContentUpload, 'path'>
    )> }
  )> }
);

export type EditResourceDataQueryVariables = {
  resourceId: Types.Scalars['String']
};


export type EditResourceDataQuery = (
  { __typename: 'RootQueryType' }
  & { resource: Types.Maybe<(
    { __typename: 'Resource' }
    & EditResourceQueryDataFragment
  )> }
);

export type EditResourceMutationVariables = {
  resource: Types.ResourceInput,
  resourceId: Types.Scalars['String'],
  icon?: Types.Maybe<Types.UploadInput>
};


export type EditResourceMutation = (
  { __typename: 'RootMutationType' }
  & { updateResource: Types.Maybe<(
    { __typename: 'Resource' }
    & EditResourceQueryDataFragment
  )> }
);

export const EditResourceQueryDataFragmentDoc = gql`
    fragment EditResourceQueryData on Resource {
  id
  language
  level
  license
  name
  subject
  summary
  format
  type
  icon {
    id
    url
    upload {
      path
    }
  }
}
    `;
export const EditResourceDataDocument = gql`
    query editResourceData($resourceId: String!) {
  resource(resourceId: $resourceId) {
    ...EditResourceQueryData
  }
}
    ${EditResourceQueryDataFragmentDoc}`;

/**
 * __useEditResourceDataQuery__
 *
 * To run a query within a React component, call `useEditResourceDataQuery` and pass it any options that fit your needs.
 * When your component renders, `useEditResourceDataQuery` returns an object from Apollo Client that contains loading, error, and data properties 
 * you can use to render your UI.
 *
 * @param baseOptions options that will be passed into the query, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options;
 *
 * @example
 * const { data, loading, error } = useEditResourceDataQuery({
 *   variables: {
 *      resourceId: // value for 'resourceId'
 *   },
 * });
 */
export function useEditResourceDataQuery(baseOptions?: ApolloReactHooks.QueryHookOptions<EditResourceDataQuery, EditResourceDataQueryVariables>) {
        return ApolloReactHooks.useQuery<EditResourceDataQuery, EditResourceDataQueryVariables>(EditResourceDataDocument, baseOptions);
      }
export function useEditResourceDataLazyQuery(baseOptions?: ApolloReactHooks.LazyQueryHookOptions<EditResourceDataQuery, EditResourceDataQueryVariables>) {
          return ApolloReactHooks.useLazyQuery<EditResourceDataQuery, EditResourceDataQueryVariables>(EditResourceDataDocument, baseOptions);
        }
export type EditResourceDataQueryHookResult = ReturnType<typeof useEditResourceDataQuery>;
export type EditResourceDataLazyQueryHookResult = ReturnType<typeof useEditResourceDataLazyQuery>;
export type EditResourceDataQueryResult = ApolloReactCommon.QueryResult<EditResourceDataQuery, EditResourceDataQueryVariables>;
export const EditResourceDocument = gql`
    mutation editResource($resource: ResourceInput!, $resourceId: String!, $icon: UploadInput) {
  updateResource(resource: $resource, resourceId: $resourceId, icon: $icon) {
    ...EditResourceQueryData
  }
}
    ${EditResourceQueryDataFragmentDoc}`;
export type EditResourceMutationFn = ApolloReactCommon.MutationFunction<EditResourceMutation, EditResourceMutationVariables>;

/**
 * __useEditResourceMutation__
 *
 * To run a mutation, you first call `useEditResourceMutation` within a React component and pass it any options that fit your needs.
 * When your component renders, `useEditResourceMutation` returns a tuple that includes:
 * - A mutate function that you can call at any time to execute the mutation
 * - An object with fields that represent the current status of the mutation's execution
 *
 * @param baseOptions options that will be passed into the mutation, supported options are listed on: https://www.apollographql.com/docs/react/api/react-hooks/#options-2;
 *
 * @example
 * const [editResourceMutation, { data, loading, error }] = useEditResourceMutation({
 *   variables: {
 *      resource: // value for 'resource'
 *      resourceId: // value for 'resourceId'
 *      icon: // value for 'icon'
 *   },
 * });
 */
export function useEditResourceMutation(baseOptions?: ApolloReactHooks.MutationHookOptions<EditResourceMutation, EditResourceMutationVariables>) {
        return ApolloReactHooks.useMutation<EditResourceMutation, EditResourceMutationVariables>(EditResourceDocument, baseOptions);
      }
export type EditResourceMutationHookResult = ReturnType<typeof useEditResourceMutation>;
export type EditResourceMutationResult = ApolloReactCommon.MutationResult<EditResourceMutation>;
export type EditResourceMutationOptions = ApolloReactCommon.BaseMutationOptions<EditResourceMutation, EditResourceMutationVariables>;


export interface EditResourceDataQueryOperation {
  operationName: 'editResourceData'
  result: EditResourceDataQuery
  variables: EditResourceDataQueryVariables
  type: 'query'
}
export const EditResourceDataQueryName:EditResourceDataQueryOperation['operationName'] = 'editResourceData'

export const EditResourceDataQueryRefetch = (
  variables:EditResourceDataQueryVariables, 
  context?:any
)=>({
  query:EditResourceDataDocument,
  variables,
  context
})
      


export interface EditResourceMutationOperation {
  operationName: 'editResource'
  result: EditResourceMutation
  variables: EditResourceMutationVariables
  type: 'mutation'
}
export const EditResourceMutationName:EditResourceMutationOperation['operationName'] = 'editResource'

export const EditResourceMutationRefetch = (
  variables:EditResourceMutationVariables, 
  context?:any
)=>({
  query:EditResourceDocument,
  variables,
  context
})
      
