import { SignUpPageCtrl } from 'ctrl/pages/signUp/SignUpPage';
import React, { FC } from 'react';
import { RouteComponentProps, RouteProps } from 'react-router-dom';
import { GuestTemplate } from 'ctrl/templates/Guest/Guest';
import { locationHelper } from './lib/helper';

interface SignupPageRouter {}
const SignupPageRouter: FC<RouteComponentProps<SignupPageRouter>> = ({ match }) => {
  return (
    <GuestTemplate withoutHeader>
      <SignUpPageCtrl />
    </GuestTemplate>
  );
};

export const SignupPageRoute: RouteProps = {
  exact: true,
  path: '/signup',
  component: SignupPageRouter
};

export const signupLocation = locationHelper<undefined, undefined>(SignupPageRoute);
