import React, { FC, useMemo } from 'react';
import { HomePageCtrl } from 'ctrl/pages/home/HomeCtrl';
import { RouteComponentProps, RouteProps } from 'react-router-dom';
import { WithSidebarTemplate } from 'ctrl/templates/WithSidebar/WithSidebar';
import { NotFoundCtrl } from 'ctrl/pages/not-found/NotFound';
import { RedirectAnonymousToLogin } from './wrappers/RedirectBySession';
import { locationHelper } from './lib/helper';

interface HomePageRouter {
  tab?: string;
}
const HomePageRouter: FC<RouteComponentProps<HomePageRouter>> = ({ match }) => {
  const homeProps: HomePageCtrl | null = useMemo(
    () => ({
      basePath: ''
    }),
    []
  );

  if (!homeProps) {
    return <NotFoundCtrl />;
  }
  return (
    <RedirectAnonymousToLogin>
      <WithSidebarTemplate>
        <HomePageCtrl {...homeProps} />
      </WithSidebarTemplate>
    </RedirectAnonymousToLogin>
  );
};

export const HomePageRoute: RouteProps = {
  exact: true,
  path: '/',
  component: HomePageRouter
};

export const homeLocation = locationHelper<undefined, undefined>(HomePageRoute);
