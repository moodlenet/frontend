import React, { FC } from 'react';
import { RouteComponentProps, RouteProps } from 'react-router-dom';
import { GuestTemplate } from 'ctrl/templates/Guest/Guest';
import { ResetPasswordPageCtrl } from 'ctrl/pages/resetPasswordRequest/resetPasswordRequestPage';
import { RedirectAuthenticatedToHome } from './wrappers/RedirectBySession';
import { locationHelper } from './lib/helper';

interface ResetPasswordPageRouter {}
const ResetPasswordPageRouter: FC<RouteComponentProps<ResetPasswordPageRouter>> = ({ match }) => {
  return (
    <RedirectAuthenticatedToHome>
      <GuestTemplate withoutHeader>
        <ResetPasswordPageCtrl />
      </GuestTemplate>
    </RedirectAuthenticatedToHome>
  );
};

export const ResetPasswordPageRoute: RouteProps = {
  exact: true,
  path: '/reset',
  component: ResetPasswordPageRouter
};

export const resetPasswordLocation = locationHelper<undefined, undefined>(ResetPasswordPageRoute);
