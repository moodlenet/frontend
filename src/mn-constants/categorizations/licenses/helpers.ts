import { Licenses, licenses, License } from '.';

export const getLicenseDesc = (licenses: Licenses, id?: string | null) =>
  getLicense(licenses, id)?.description || 'unknown';

export const getLicense = (licenses: Licenses, id?: string | null): License | undefined =>
  licenses.find(_ => _.id === id);

export const getLicenseOption = (licenses: Licenses, id?: string | null) => {
  const license = getLicense(licenses, id);
  return (
    license && {
      label: license.description,
      value: license.id
    }
  );
};

export const defaultLicense = getLicenseOption(licenses, 'CC0')!;
