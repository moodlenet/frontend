export interface Format {
  id: string;
  description: string;
  group: string;
}
export type Formats = Format[];

export const formats: Formats = [
  { group: 'Generic', description: 'Unspecified', id: 'unspecified' },
  { group: 'Generic', description: 'Audio', id: 'audio' },
  { group: 'Generic', description: 'H5P', id: 'h5p' },
  { group: 'Generic', description: 'Image', id: 'image' },
  { group: 'Generic', description: 'IMS cartridge', id: 'imscartridge' },
  { group: 'Generic', description: 'LTI cartridge', id: 'lticartridge' },
  { group: 'Generic', description: 'Portal / main site', id: 'portalmainsite' },
  { group: 'Generic', description: 'SCORM', id: 'scorm' },
  { group: 'Generic', description: 'Text', id: 'text' },
  { group: 'Generic', description: 'Video / animation', id: 'videoanimation' },
  { group: 'Moodle', description: 'Moodle Assignment', id: 'moodleassignment' },
  { group: 'Moodle', description: 'Moodle Book', id: 'moodlebook' },
  { group: 'Moodle', description: 'Moodle Certificate', id: 'moodlecertificate' },
  { group: 'Moodle', description: 'Moodle Choice', id: 'moodlechoice' },
  { group: 'Moodle', description: 'Moodle community plugin', id: 'moodlecommunityplugin' },
  { group: 'Moodle', description: 'Moodle Course', id: 'moodlecourse' },
  {
    group: 'Moodle',
    description: 'Moodle Database and DB presets',
    id: 'moodledatabaseanddbpresets'
  },
  { group: 'Moodle', description: 'Moodle Feedback', id: 'moodlefeedback' },
  {
    group: 'Moodle',
    description: 'Moodle Glossary and XML lists',
    id: 'moodleglossaryandxmllists'
  },
  { group: 'Moodle', description: 'Moodle Lesson', id: 'moodlelesson' },
  { group: 'Moodle', description: 'Moodle Questionnaire', id: 'moodlequestionnaire' },
  {
    group: 'Moodle',
    description: 'Moodle Quiz and Quiz questions',
    id: 'moodlequizandquizquestions'
  },
  { group: 'Moodle', description: 'Moodle Wiki', id: 'moodlewiki' },
  { group: 'Moodle', description: 'Moodle Workshop', id: 'moodleworkshop' },
  { group: 'Moodle_Workplace', description: 'Certificate', id: 'certificate' },
  { group: 'Moodle_Workplace', description: 'Certifications', id: 'certifications' },
  { group: 'Moodle_Workplace', description: 'Custom reports', id: 'customreports' },
  { group: 'Moodle_Workplace', description: 'Dynamic rules', id: 'dynamicrules' },
  {
    group: 'Moodle_Workplace',
    description: 'Organisation structure frameworks',
    id: 'organisationstructureframeworks'
  },
  {
    group: 'Moodle_Workplace',
    description: 'Organisation structure jobs',
    id: 'organisationstructurejobs'
  },
  { group: 'Moodle_Workplace', description: 'Program', id: 'program' }
];
export default formats;
