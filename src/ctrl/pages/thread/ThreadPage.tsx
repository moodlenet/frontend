import { t } from '@lingui/macro';
import { usePageTitle } from 'context/global/pageCtx';
import { useThreadComments } from 'fe/comment/thread/useThreadComments';
import { getActivityActor } from 'fe/lib/activity/getActivityActor';
import { getActivityMainContext } from 'fe/lib/activity/getActivityMainContext';
import { getCommunityInfoStrings } from 'fe/lib/activity/getContextCommunityInfo';
import { useThreadPreview } from 'fe/thread/preview/useThreadPreview';
import { useFormik } from 'formik';
import { Thread } from 'graphql/types.generated';
import { PreviewIndex } from 'ctrl/modules/previews';
import { CommentPreviewCtrl } from 'ctrl/modules/previews/comment/CommentPreview';
import { SocialTextCtrl } from 'ctrl/modules/SocialText/SocialTextCtrl';
import React, { FC, useMemo } from 'react';
import { Box } from 'rebass';
import {
  ActivityLoaded as ActivityPreviewProps,
  ActivityPreview,
  Status as ActivityPreviewStatus
} from 'ui/modules/ActivityPreview';
import { Props, Thread as ThreadPageUI } from 'ui/pages/thread';

export interface ThreadPage {
  threadId: Thread['id'];
}
const threadPageTitle = t`Discussion: {excerpt}`;

export const ThreadPage: FC<ThreadPage> = ({ threadId }) => {
  const { commentPage } = useThreadComments(threadId);
  const [loadMoreComments] = commentPage.formiks;
  const thread = useThreadPreview(threadId);
  const titleValues = thread.mainComment && {
    excerpt: `${thread.mainComment.content.substring(0, 30)} ...`
  };
  usePageTitle(!!titleValues && threadPageTitle, titleValues);
  const replyFormik = useFormik<{ replyMessage: string }>({
    initialValues: { replyMessage: '' },
    onSubmit: ({ replyMessage }) => thread.reply(replyMessage)
  });

  const uiProps = useMemo<null | Props>(() => {
    const { context: threadContext, mainComment } = thread;
    const context = getActivityMainContext(threadContext);
    if (!(mainComment && context)) {
      return null;
    }

    const { communityName, communityIcon, communityLink } = getCommunityInfoStrings(context);

    const activityProps: Pick<
      ActivityPreviewProps,
      'status' | 'communityLink' | 'communityName' | 'event'
    > = {
      communityLink,
      communityName,
      event: 'started a discussion',
      status: ActivityPreviewStatus.Loaded
    };

    const MainThread = (
      <ActivityPreview
        {...{
          ...activityProps,
          actor: mainComment.creator && getActivityActor(mainComment.creator),
          preview: <CommentPreviewCtrl commentId={mainComment.id} mainComment={true} />,
          createdAt: mainComment.createdAt
        }}
      />
    );

    const Comments = (
      <>
        {commentPage.edges
          .filter(comment => comment.id !== thread.mainComment?.id)
          .map(comment => (
            <Box mb={1} key={comment.id}>
              <ActivityPreview
                {...{
                  ...activityProps,
                  event: 'replied',
                  actor: comment.creator && getActivityActor(comment.creator),
                  preview: (
                    <CommentPreviewCtrl
                      commentId={comment.id}
                      mainComment={false}
                      hideActions={true}
                    />
                  ),
                  createdAt: comment.createdAt
                }}
              />
            </Box>
          ))}
      </>
    );

    const Context = (
      <PreviewIndex
        ctx={context.__typename === 'User' ? { ...context, id: context.userId } : context}
      />
    );

    const SocialTextElement = thread.canReply ? (
      <SocialTextCtrl
        submit={replyFormik.submitForm}
        textChange={replyMessage => replyFormik.setValues({ replyMessage })}
      />
    ) : null;

    const props: Props = {
      SocialTextElement,
      Comments,
      Context,
      MainThread,
      communityIcon,
      communityLink,
      communityName,
      isCommunityContext: thread.context?.__typename === 'Community',
      loadMoreComments
    };

    return props;
  }, [thread, commentPage.edges, replyFormik, loadMoreComments]);
  return uiProps && <ThreadPageUI {...uiProps} />;
};
