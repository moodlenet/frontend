import { t } from '@lingui/macro';
import { usePageTitle } from 'context/global/pageCtx';
import React, { FC } from 'react';
import { NotFound } from 'ui/pages/notFound';

export interface NotFoundCtrl {}

const notFoundPageTitle = t`Page Not Found`;

export const NotFoundCtrl: FC<NotFoundCtrl> = () => {
  usePageTitle(notFoundPageTitle, {});
  return <NotFound />;
};
