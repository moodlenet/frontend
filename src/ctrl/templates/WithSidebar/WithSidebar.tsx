import { useMe } from 'fe/session/useMe';
import { MainHeaderCtrl } from 'ctrl/modules/Header/Header';
import { SidebarCtrl } from 'ctrl/modules/Sidebar/Sidebar';
import React, { FC, useMemo } from 'react';
import { SidebarProps, WithSidebar } from 'ui/templates/withSidebar';
import { GuestTemplate } from '../Guest/Guest';
import { ProvideSideBarContext } from 'ctrl/context/SideBar';
import { SearchBox } from 'ctrl/modules/SearchBox/SearchBox';
import { userLocation } from 'routes/UserPageRoute';
import { homeLocation } from 'routes/HomePageRoute';

export interface WithSidebarTemplate {}
export const WithSidebarTemplate: FC<WithSidebarTemplate> = ({ children }) => {
  const { loading, me, logout } = useMe();

  const withSidebarProps = useMemo<null | SidebarProps>(() => {
    const user = me?.user;
    if (!user || loading) {
      return null;
    }
    const userImage = user.icon?.url || '';
    const userLink = userLocation.getPath({ tab: undefined, userId: user.id }, undefined);
    const props: SidebarProps = {
      SidebarBox: <SidebarCtrl />,
      HeaderBox: <MainHeaderCtrl />,
      SearchBox: <SearchBox />,
      userImage,
      userLink,
      signout: logout,
      homeLink: homeLocation.getPath(undefined, undefined),
      username: user.displayUsername || '',
      name: user.preferredUsername || ''
    };
    return props;
  }, [loading, logout, me]);
  // console.log('withSidebarProps', withSidebarProps);
  return withSidebarProps ? (
    <ProvideSideBarContext>
      <WithSidebar {...withSidebarProps}>{children}</WithSidebar>
    </ProvideSideBarContext>
  ) : (
    <GuestTemplate>{children}</GuestTemplate>
  );
};
