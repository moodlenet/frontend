import * as Types from '../../../../graphql/types.generated';

import { CommunityInfoFragment } from '../community/CommunityPreview.generated';
import gql from 'graphql-tag';
import { CommunityInfoFragmentDoc } from '../community/CommunityPreview.generated';


export type ResourcePreviewFragment = (
  { __typename: 'Resource' }
  & Pick<Types.Resource, 'id' | 'name' | 'summary' | 'canonicalUrl' | 'likerCount' | 'language' | 'level' | 'subject' | 'format' | 'type' | 'license'>
  & { icon: Types.Maybe<(
    { __typename: 'Content' }
    & Pick<Types.Content, 'id' | 'url'>
    & { upload: Types.Maybe<(
      { __typename: 'ContentUpload' }
      & Pick<Types.ContentUpload, 'path'>
    )> }
  )>, payload: Types.Maybe<(
    { __typename: 'Content' }
    & Pick<Types.Content, 'id' | 'mediaType' | 'url'>
    & { mirror: Types.Maybe<(
      { __typename: 'ContentMirror' }
      & Pick<Types.ContentMirror, 'url'>
    )>, upload: Types.Maybe<(
      { __typename: 'ContentUpload' }
      & Pick<Types.ContentUpload, 'size' | 'path'>
    )> }
  )>, myLike: Types.Maybe<(
    { __typename: 'Like' }
    & Pick<Types.Like, 'id'>
  )>, myFlag: Types.Maybe<(
    { __typename: 'Flag' }
    & Pick<Types.Flag, 'id'>
  )>, creator: Types.Maybe<(
    { __typename: 'User' }
    & Pick<Types.User, 'id'>
  )>, collection: Types.Maybe<(
    { __typename: 'Collection' }
    & Pick<Types.Collection, 'id' | 'name' | 'preferredUsername' | 'canonicalUrl' | 'summary' | 'resourceCount'>
    & { icon: Types.Maybe<(
      { __typename: 'Content' }
      & Pick<Types.Content, 'id' | 'url'>
    )>, community: Types.Maybe<(
      { __typename: 'Community' }
      & Pick<Types.Community, 'id'>
      & CommunityInfoFragment
    )> }
  )> }
);

export const ResourcePreviewFragmentDoc = gql`
    fragment ResourcePreview on Resource {
  id
  icon {
    id
    url
    upload {
      path
    }
  }
  name
  summary
  canonicalUrl
  payload: content {
    id
    mediaType
    mirror {
      url
    }
    upload {
      size
      path
    }
    url
  }
  myLike {
    id
  }
  myFlag {
    id
  }
  likerCount
  language
  level
  subject
  format
  type
  creator {
    id
  }
  collection {
    id
    name
    preferredUsername
    canonicalUrl
    icon {
      id
      url
    }
    summary
    community {
      id
      ...CommunityInfo
    }
    resourceCount
  }
  license
}
    ${CommunityInfoFragmentDoc}`;
