import React, { FC } from 'react';
import {
  Activity,
  Collection,
  Comment,
  Community,
  Resource,
  Thread,
  User
} from 'graphql/types.generated';
import { ActivityPreviewCtrl } from './activity/ActivityPreview';
import { CollectionPreviewCtrl } from './collection/CollectionPreview';
import { CommentPreviewCtrl } from './comment/CommentPreview';
import { CommunityPreviewCtrl } from './community/CommunityPreview';
import { ResourcePreviewCtrl } from './resource/ResourcePreview';
import { ThreadPreviewCtrl } from './thread/ThreadPreview';
import { UserPreviewCtrl } from './user/UserPreview';
import Maybe from 'graphql/tsutils/Maybe';

export type Ctx = Pick<
  Activity | Collection | Comment | Community | Resource | Thread | User,
  'id' | '__typename'
>;
interface PreviewIndex {
  ctx: Maybe<Ctx>;
}

export const PreviewIndex: FC<PreviewIndex> = ({ ctx }) => {
  if (!ctx) {
    return null;
  }
  switch (ctx.__typename) {
    case 'Activity':
      return <ActivityPreviewCtrl activityId={ctx.id} />;
    case 'Collection':
      return <CollectionPreviewCtrl collectionId={ctx.id} />;
    case 'Comment':
      return <CommentPreviewCtrl commentId={ctx.id} mainComment={false} />;
    case 'Community':
      return <CommunityPreviewCtrl communityId={ctx.id} />;
    case 'Resource':
      return <ResourcePreviewCtrl resourceId={ctx.id} />;
    case 'Thread':
      return <ThreadPreviewCtrl threadId={ctx.id} />;
    case 'User':
      return <UserPreviewCtrl userId={ctx.id} />;
    default:
      return null;
  }
};
