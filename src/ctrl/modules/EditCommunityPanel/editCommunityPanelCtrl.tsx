import { useEditCommunity } from 'fe/community/edit/useEditCommunity';
import { useFormik } from 'formik';
import { Community } from 'graphql/types.generated';
import React, { FC, useMemo } from 'react';
import { EditCommunityFormValues, EditCommunityPanel } from 'ui/modules/EditCommunityPanel';
import * as Yup from 'yup';
import { TestUrlOrFile } from 'ctrl/lib/formik-validations';
import { DropzoneAreaCtrl } from '../DropzoneArea/DropzoneAreaCtrl';

interface EditCommunityFormValuesWithIcon extends EditCommunityFormValues {
  icon: File | string | undefined;
}
export const validationSchema: Yup.ObjectSchema<EditCommunityFormValuesWithIcon> = Yup.object<
  EditCommunityFormValuesWithIcon
>({
  name: Yup.string()
    .min(2)
    .max(60)
    .required(),
  summary: Yup.string().max(500),
  icon: Yup.mixed<File | string>().test(...TestUrlOrFile)
});

export interface Props {
  communityId: Community['id'];
  done(): any;
}
export const EditCommunityPanelCtrl: FC<Props> = ({ done, communityId }: Props) => {
  const { community, edit } = useEditCommunity(communityId);
  const initialValues = useMemo<EditCommunityFormValuesWithIcon>(
    () => ({
      icon: community?.icon?.url || '',
      name: community?.name || '',
      summary: community?.summary || ''
    }),
    [community]
  );

  const formik = useFormik<EditCommunityFormValuesWithIcon>({
    enableReinitialize: true,
    onSubmit: vals => {
      return edit({
        community: {
          name: vals.name,
          summary: vals.summary || undefined
        },
        icon: vals.icon
      }).then(done);
    },
    validationSchema,
    initialValues
  });
  const IconElement = (
    <DropzoneAreaCtrl
      imageOnly
      imageUrl={community?.icon?.url}
      onFileSelected={icon => formik.setValues({ ...formik.values, icon }, false)}
    />
  );
  return <EditCommunityPanel IconElement={IconElement} cancel={done} formik={formik} />;
};
