import Fuse from 'fuse.js';
import { grades, languages, subjects, types, licenses, formats } from 'mn-constants';
import React, { FC, useMemo, useRef, useState } from 'react';
import { FormBag } from 'ui/@types/types';
import {
  Option,
  ResourceCategorization,
  ResourceCategorizationValues,
  OptionGroup
} from 'ui/modules/ResourceCategorization';
import * as Yup from 'yup';

export type ResourceCategorizationType = ResourceCategorizationValues;

export interface ResourceCategorizationCtrl {
  formik: FormBag<ResourceCategorizationValues>;
}

export const getCategorizationValues = (formValues: ResourceCategorizationValues) => ({
  grade: formValues.grade.value,
  language: formValues.language.value,
  subject: formValues.subject.value,
  type: formValues.type.value
});
export const emptyCategorizationInitialValues: ResourceCategorizationValues = {
  // grade: defaultGrade,
  // language: defaultLanguage,
  // subject: defaultSubject,
  // type: defaultType
} as ResourceCategorizationValues;
export const categorizationSchema = {
  format: Yup.object<Option>().required(),
  grade: Yup.object<Option>().required(),
  language: Yup.object<Option>().required(),
  subject: Yup.object<Option>().required(),
  type: Yup.object<Option>().required(),
  license: Yup.object<Option>().required()
};

export const ResourceCategorizationCtrl: FC<ResourceCategorizationCtrl> = ({ formik }) => {
  // console.table(formik.values);
  const [searchs, setSearchs] = useState({
    grade: '',
    format: '',
    language: '',
    subject: '',
    type: ''
  });

  const { current: searchInputChange } = useRef<ResourceCategorization['searchInputChange']>(
    field => search => {
      setSearchs({ ...searchs, [field]: search });
    }
  );

  const filteredGrades = useMemo(
    () => (searchs.grade ? gradeFuse.search(searchs.grade).map(_ => _.item) : gradeOptions),
    [searchs.grade]
  );
  const filteredLanguages = useMemo(
    () =>
      searchs.language ? languageFuse.search(searchs.language).map(_ => _.item) : languageOptions,
    [searchs.language]
  );
  const filteredSubjects = useMemo(
    () => (searchs.subject ? subjectFuse.search(searchs.subject).map(_ => _.item) : subjectOptions),
    [searchs.subject]
  );
  const filteredTypes = useMemo(
    () => (searchs.type ? typeFuse.search(searchs.type).map(_ => _.item) : typeOptions),
    [searchs.type]
  );

  return (
    <ResourceCategorization
      searchInputChange={searchInputChange}
      formik={formik}
      grades={filteredGrades}
      languages={filteredLanguages}
      subjects={filteredSubjects}
      types={filteredTypes}
      licenses={licenseOptions}
      formats={formatOptions}
    />
  );
};

const fuseOptions: Fuse.IFuseOptions<Option> = {
  keys: ['label', 'value'],
  minMatchCharLength: 1,
  findAllMatches: true,
  threshold: 1,
  //ignoreLocation: true,
  shouldSort: true,
  distance: 10
};
const gradeOptions: ResourceCategorization['grades'] = grades.map(grade => ({
  label: grade.description,
  value: grade.id
}));
const gradeFuse = new Fuse(gradeOptions, fuseOptions);

const languageOptions: ResourceCategorization['languages'] = languages.map(lang => ({
  label: lang.isoName,
  value: lang.code
}));
const languageFuse = new Fuse(languageOptions, fuseOptions);

const subjectOptions: ResourceCategorization['subjects'] = subjects.map(subject => ({
  label: subject.description,
  value: subject.id
}));
const subjectFuse = new Fuse(subjectOptions, fuseOptions);

const typeOptions: ResourceCategorization['types'] = types.map(type => ({
  label: type.description,
  value: type.id
}));
const typeFuse = new Fuse(typeOptions, fuseOptions);

const licenseOptions: OptionGroup[] = Object.values(
  licenses.reduce((optionGroups, license) => {
    const group: OptionGroup = optionGroups[license.type] || { label: license.type, options: [] };
    group.options.push({ label: license.description, value: license.id });
    return {
      ...optionGroups,
      [license.type]: group
    };
  }, {} as Record<string, OptionGroup>)
);

const formatOptions: OptionGroup[] = Object.values(
  formats.reduce((optionGroups, format) => {
    const group: OptionGroup = optionGroups[format.group] || { label: format.group, options: [] };
    group.options.push({ label: format.description, value: format.id });
    return {
      ...optionGroups,
      [format.group]: group
    };
  }, {} as Record<string, OptionGroup>)
);
