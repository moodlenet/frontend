# MoodleNet Client Architecture

## Overview

MoodleNet Client is a SPA developed in [Typescript lang](https://www.typescriptlang.org/) built on top of 3 main libraries

- [React](https://reactjs.org/) (for DOM manipulation)
- [Apollo](https://www.apollographql.com/) (for managing GraphQL protocol)
- [Lingui](https://lingui.js.org/) (for I18n)

The codebase is fully and scrictly typed to guarantee:

- high degree of bug-catching at compile time
- easy refactoring
- dev sweets
- reliability

Project has been scaffolded by [create-react-app](https://create-react-app.dev/) with [CRA Typescript](https://create-react-app.dev/docs/adding-typescript/) template

GraphQL typings and typed react-hooks API modules based on custom GQL queries are generated using [GraphQL Code Generator](https://graphql-code-generator.com/) ensuring, to great extent, type safety in GraphQL realm too

Project is intended to be developed on [VSCode](https://code.visualstudio.com/) for its great Typescript support
a couple of vs-code tasks useful for development (eslint and tsc, in watch mode) are shipped within the codebase

The Application codebase so far, is composed by:

A 3 layered MVP(ish) pattern ...

- **UI (V) `src/ui/`** : Stateless React components, they are the building blocks of the app UI
- **Controllers (P) `src/ctrl/`** : Stateful React components wrapping and controlling `UI`s, dependending on `FrontEnd`
- **FrontEnd (M) `src/fe/`** : Modules providing applicative service-hooks to controller components, mostly based on GraphQL queries/mutations

.. and a bunch of main global services:

- **Apollo Client `src/apollo/`** : main `ApolloClient` configuration and instantiation
- **Contexts `src/context/`** : global application `React.Context`s - Localization, Algolia, Page, ...(more to come)
- **Routes `src/routes/`** : Stateless React components interfacing `react-router` APIs to routable page-controllers

## Application Layers

MoodleNet client adopted a neat separation of UI and data-model|business-logic code with a MVP(ish) flavour.

### MVP

#### V: UI

UI components are stateless functions and they are commonly wrapped and controlled by a controller.

All UI components are located in `src/ui/` folder, and are intended to be developed with a high degree of independence from application logic and BackEnd services/data model.

UI development leverages [Storybook](https://storybook.js.org/) to autonomously develop, test and present stateless UI building blocks.

Each UI component implements `React.FC<Props>` function, defining its own `Props` interface containing all necessary data and callbacks needed for the UI view.

No state is allowed, and nearly no logic is present in UI comnponents except for those necessary for rendering (e.g. `Array.map()` rendering iterations or some conditionals).

Moreover, UIs typically do not import and use other reusable UIs directly.

Instead, they define their `Props` to contain `ReactElement`s - or lists of - wich are supposed to be controlled reusable UI components and render them directly in proper slots.

This approach makes possible to develop higher level stateless UI components - using reusable stateless components - presentable in Storybook without any dependency.

#### P: Controllers

Controller components are located in `src/ctrl/` folder, each controls a specific UI component and glues it to the application layers.
Each controller simply provides the UI its depending `Props` properly, implementing:

- UI state and business logic
- Two ways mappings between BackEnd <-> UI data models
- UI events handling (callback props)

Controllers use `FE` hooks and usually define a `.graphql` file defining fragments of data they require to properly feed the UI and manage state & logic.

#### M: Front End

Front End (FE) modules are located in `src/fe` folder.

FE modules provide controllers reusable hooks for data fetching and mutations, which mostly are handled using generated GraphQL-hooks, but may also use local application services and state provided by `Contexts`.

Some data pre-digesting may occour in FE when makes sense.

For GraphQL related APIs FE modules are paired with a `.graphql` file defining query|mutations and importing controllers `.graphql` fragment definitions to ensure exhaustiveness of selections.

### Contexts

Currently available global contexts are:

- Algolia (src/context/global/algolia.tsx) configures and provides `InstantSearch` for algolia search features
- Localization (src/context/global/localizationCtx.tsx) configures and provides `lingui` features
- Page (src/context/global/algolia.tsx) provides simple APIs to modify page headers (currently only title but more to come)

### Routes

`src/routes/` folder contains simple tsx modules defining conventionally exporting a set of properties useful for page routing management across the app.

more on routes [here](./ROUTES.md)
