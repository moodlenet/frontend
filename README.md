# MoodleNet Web Client

This React project was bootstrapped with [Create React App](https://create-react-app.dev/) with [CRA Typescript](https://create-react-app.dev/docs/adding-typescript/) template.

## Index

- [Deploying MoodleNet](#deploying-moodlenet)
- [Structure](#structure)
  - [High level folder structure](#high-level-folder-structure)
  - [Application source folder structure](#application-source-folder-structure)
- [Development Scripts](#development-scripts)
  - [`yarn start`](#yarn-start)
  - [`yarn add-locale`](#yarn-add-locale)
  - [`yarn extract`](#yarn-extract)
  - [`yarn compile`](#yarn-compile)
  - [`yarn build`](#yarn-build)
- [Libraries](#libraries)

## Deploying MoodleNet

### Install using Docker containers (recommended)

1. Make sure you have [Docker](https://www.docker.com/), a recent [docker-compose](https://docs.docker.com/compose/install/#install-compose) (which supports v3 configs) installed:

```sh
$ docker version
Docker version 18.09.1-ce
$ docker-compose -v
docker-compose version 1.23.2
...
```

2. Clone this repository and change into the directory:

```sh
$ git clone https://gitlab.com/moodlenet/frontend.git moodlenet
...
$ cd moodlenet
```

3. Configuration

First make sure to configure your domain name or subdomain to point to your server's IP address.

We need to set some environment variables in order for MoodleNet to function, a list of which can be found in these files: - `.env` - If you have a domain configured to point to your server, replace every instance of 'localhost' with 'your-domain-here.tld' and those of 'http:' with 'https:' (the 's' is critical) - **If you want to connect your instance with the MoodleNet "mothership" for indexing public content, search, and discovery**, and you agree with the [Terms for Instance Administrators](https://moodle.net/terms/admins/index.html), set CONNECT_WITH_MOTHERSHIP to true, otherwise set it to false. You should then email moodlenet-moderators@moodle.com to request an API key. - `.env.secrets.example` (which you must copy to `.env.secrets`) - set each password and secret with something random and secure - MAIL_DOMAIN and MAIL_KEY are needed to configure transactional email, sign up at [Mailgun](https://www.mailgun.com/) and then configure the domain name and key

4. Once configured, build the docker image:

```sh
$ docker-compose build
...
```

Or if you're building on a Raspberry Pi:

```sh
$ docker-compose -f docker-compose.pi.yml build
...
```

5. Try it out

a) run the backend in console mode: `docker-compose run --rm backend bin/moodle_net start_iex`

b) if you're in invite-only mode, add your email to the allowlist in order to be able to sign up: `MoodleNet.Access.create_register_email("myemail@domain.com")` and then exit (Ctrl+C and then `abort`)

c) Start the docker containers with docker-compose:

```sh
$ docker-compose up
...
```

Or if you're running on a Raspberry Pi:

```sh
$ docker-compose -f docker-compose.pi.yml up
...
```

6. The MoodleNet backend and frontend should now be running at [http://localhost/](http://localhost/) on your machine and at `https://your-domain-name.tld/` with SSL certificates automatically configured thanks to letsencrypt.org (if your domain was correctly configured).
   Once you've signed up, you may want to make yourself an instance admin, by running this in the backend console (see above): `MoodleNet.ReleaseTasks.make_instance_admin("your_username")`

7. If that worked, start the app as a daemon next time:

```sh
$ docker-compose up -d
...
```

Or if you're running on a Raspberry Pi:

```sh
$ docker-compose -f docker-compose.pi.yml up -d
...
```

## Structure

### High level folder structure

| Folder    | Description                                                       |
| --------- | ----------------------------------------------------------------- |
| `/build`  | the output directory containing static assets & application files |
| `/public` | files that will be copied into the `build` folder                 |
| `/src`    | the application source                                            |

### Application source folder structure

| Folder              | Description                                                                                                                  |
| ------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| `/src/apollo`       | all (react-)apollo boilerplate, type definitions, and resolvers                                                              |
| `/src/containers`   | high-level react container components which handle routing, state, and main set-ups                                          |
| `/src/graphql`      | contains main schema generated typings                                                                                       |
| `/src/locales`      | locale folders define the available locales (managed by linguijs) and each contains its locale's language data               |
| `/src/static`       | static assets such as images that are used within the application code (for example, images can be `require`'d with webpack) |
| `/src/ui`           | UI components                                                                                                                |
| `/src/ctrl`         | Controllers components for UI                                                                                                |
| `/src/routes`       | Routing components interfacing React-Router to main Page-Controllers                                                         |
| `/src/fe`           | Controller's hooks APIs                                                                                                      |
| `/src/mn-constants` | Hard-coded and build-env constants                                                                                           |

## Development Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.
You will also see any lint errors in the console.

### `yarn add-locale`

Adds a locale for localisation, with [lingui library](https://lingui.js.org/ref/react.html).

### `yarn extract`

Extracts new/updated strings from the codebase into JSON files for localisation (they need to be encapsulated with [lingui library](https://lingui.js.org/ref/react.html)'s `<Trans>`).

### `yarn compile`

Compiles localisation files for production.

### `yarn build`

Builds the app for production to the `build` folder.
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.

### `yarn storybook`

Builds a `Storybook` static app out of the storybook's `tsx`|`mdx` modules associated to UI components

### `yarn gqlcode`

Generates typed modules based on `src/**/*.graphql` files and `.env.development`'s `REACT_APP_GRAPHQL_ENDPOINT` Backend GraphQL exposed schema.

## Libraries

This section mentions notable libraries and tools used within the application and provides links to their documentation.

- React, UI framework [https://reactjs.org/docs/getting-started.html](https://reactjs.org/docs/getting-started.html)
- TypeScript, programming language [https://www.typescriptlang.org](https://www.typescriptlang.org)
- webpack, build tool [https://webpack.js.org](https://webpack.js.org)
- React Apollo, GraphQL client [https://www.apollographql.com/docs/react/](https://www.apollographql.com/docs/react/)
- Phoenix.js, Phoenix channels JS client [https://hexdocs.pm/phoenix/js/index.html](https://hexdocs.pm/phoenix/js/index.html)
- GraphQL Code Generator [https://graphql-code-generator.com/](https://graphql-code-generator.com/)
- Formik form management [https://formik.org/](https://formik.org/)
- LinguiJs localisation library [https://lingui.js.org/](https://lingui.js.org/)
